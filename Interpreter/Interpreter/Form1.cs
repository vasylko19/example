﻿//--------------------------------------------------------------------------
using System;
using System.Windows.Forms;
//--------------------------------------------------------------------------
namespace Interpretator
{
    //----------------------------------------------------------------------
    //main window of the program
    //----------------------------------------------------------------------
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //------------------------------------------------------------------
        private void MagicButton_Click(object sender, EventArgs e)
        {
            Console.Text = "";
            SyntaxxAnalyzer.AnalyzerOutput = "";
            try
            {
                LexicAnalyzerr lexer = new LexicAnalyzerr();
                var tokens = lexer.Analyze(TextBox.Lines);
                
                SyntaxxAnalyzer syntaxAnalyzer = new SyntaxxAnalyzer();
                IExpression tree = syntaxAnalyzer.TreeBuilderr(tokens);
				
				tree.Execute(new Contextt());

				Console.Clear();
                Console.Text += SyntaxxAnalyzer.AnalyzerOutput;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

		private void NotMagicButton_Click(object sender, EventArgs e)
		{
			Console.Clear();
			SyntaxxAnalyzer.AnalyzerOutput = "";
			try
			{
				LexicAnalyzerr lexer = new LexicAnalyzerr();
				var tokens = lexer.Analyze(TextBox.Lines);

				foreach (var token in tokens)
				{
					Console.AppendText(token.ToString() + "\n");
				}

				SyntaxxAnalyzer syntaxAnalyzer = new SyntaxxAnalyzer();
				IExpression tree = syntaxAnalyzer.TreeBuilderr(tokens);

				Console.AppendText(new TreePrinter().PrintTree(tree));
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}
	}
}
//--------------------------------------------------------------------------
