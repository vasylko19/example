﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class PlussExpressions : IExpression
    {
        public IExpression LeftOperandd { get; private set; }
        public IExpression RightOperandd { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            var left = LeftOperandd.Execute(context);
            var right = RightOperandd.Execute(context);
            
            if (left.GetType() != right.GetType())
            {
                throw new Exception($"PlusException: Different variables types: '{left.GetType()}' and '{right.GetType()}'!");
            }
            else return (object) ((dynamic) left + (dynamic) right);
        }
        //----------------------------------------------------------------------
        public PlussExpressions(IExpression left, IExpression right)
        {
            LeftOperandd = left;
            RightOperandd = right;
        }
    }
    //--------------------------------------------------------------------------
    class PlusExpressionsCreatorr : IExpressionCreator
    {
       
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            var startPosition = position;
            if (tokens[position].getType() != TokenType.LeftBracket)
                return null;

            position++;
            var leftOperand = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.Plus)
            {
                position = startPosition;
                return null;
            }

            position++;
            var rightOPerand = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() == TokenType.RightBracket)
            {
                position++;
                return new PlussExpressions(leftOperand, rightOPerand);
            }
            else
            {
                position = startPosition;
                throw new Exception("SyntaxErrorException in 'PLUS' declaration!");
            }
        }

        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public PlusExpressionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
    }


}
//------------------------------------------------------------------------------