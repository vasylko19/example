﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //assigns right operand to the left operand
    //--------------------------------------------------------------------------
    class AssignmentExpressions : IExpression
    {
        public string LeftOperandd { get; private set; }
		public IExpression RightOperand { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            object value;
            //checking if left operand exists in the dictionary
            if (context.Variables.ContainsKey(LeftOperandd))
            {
                value = RightOperand.Execute(context);
                //cheking if both operands are the same type
                if (context.Variables[LeftOperandd].GetType() == value.GetType())
                    context.Variables[LeftOperandd] = value;
                else throw new Exception($"Error! Different types of operands!");
            }
            else throw new Exception($"Variable {LeftOperandd} do not exist!"); 
            
            return value;
        }
        //----------------------------------------------------------------------
        public AssignmentExpressions(string left, IExpression right)
        {
            RightOperand = right;
            LeftOperandd = left;
        }
    }
    //--------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------
    class AssignmenCreatorr : IExpressionCreator
    {
        private readonly SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public AssignmenCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            var leftOperand = tokens[position];
            //checking tokens values
            if (leftOperand.getType() != TokenType.VariableName || tokens[position + 1].getType() != TokenType.Assign)
                return null;
            else
            {
                position+=2;
                var rightOperand = _analyzer.GetCurrentExpressions(tokens, ref position);
                return new AssignmentExpressions(leftOperand.GetLexem(), rightOperand);
            }
        }
    }
}
//------------------------------------------------------------------------------