﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class IfElseExpressions : IExpression
    {
        public IExpression Condition { get; private set; }
		public IExpression TrueExpressions { get; private set; }
		public IExpression FalseExpressions { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            bool condition = Convert.ToBoolean(Condition.Execute(context));

            if (condition) return TrueExpressions.Execute(context);
            else
            {
                if (FalseExpressions == null)
                    return null;
                else return FalseExpressions.Execute(context);
            }
        }
        //----------------------------------------------------------------------
        public IfElseExpressions(IExpression condition, IExpression trueExpression, IExpression falseExpression)
        {
            TrueExpressions = trueExpression;
            Condition = condition;
            FalseExpressions = falseExpression;
        }
    }
    //--------------------------------------------------------------------------
    class IfElseExpressionsCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public IfElseExpressionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //ckecking if the token == "if"
            if (tokens[position].getType() != TokenType.If)
                return null;
            //checking if there is "(" after "if"
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"If statement syntax error: '(' is missing!");
            //analyzing the expression
            position++;
            var condition = _analyzer.GetCurrentExpressions(tokens, ref position);
            //checking if there is ")" after "if"
            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"If statement syntax error: ')' is missing!");
            //adding all the tokens to the tree
            position++;
            var tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.Else && tokens[position].getType() != TokenType.End)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            //building the tree
            var trueExpression = new SyntaxxAnalyzer().TreeBuilderr(tokensList);
            //if we've come to an end executing the "true" expression
            if (tokens[position].getType() == TokenType.End)
            {
                position++;
                return new IfElseExpressions(condition, trueExpression, null);
            }
            else if (tokens[position].getType() != TokenType.Else)
                throw new Exception($"If statement syntax error: 'end' is missing!");
            
            position++;
            tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.End)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            var falseExpression = new SyntaxxAnalyzer().TreeBuilderr(tokensList);

            if (tokens[position].getType() != TokenType.End)
                throw new Exception($"If statement syntax error: keyword 'end' is missing!");
            else
            {
                position++;
                return new IfElseExpressions(condition, trueExpression, falseExpression);
            }
        }
    }
}
//------------------------------------------------------------------------------