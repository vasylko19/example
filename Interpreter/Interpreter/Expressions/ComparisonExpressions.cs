﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{


    class ComparisonCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public ComparisonCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            string sign;
            var startPosition = position;
            //checking if there are brackets around the expression
            if (tokens[position].getType() != TokenType.LeftBracket)
                return null;
            //getting left operand
            position++;
            var leftOperand = _analyzer.GetCurrentExpressions(tokens, ref position);
            //checking token's type
            if (tokens[position].getType() != TokenType.Less &&
                tokens[position].getType() != TokenType.EqualLess &&
                tokens[position].getType() != TokenType.NotEqual &&
                tokens[position].getType() != TokenType.Equal &&
                tokens[position].getType() != TokenType.EqualMore &&
                tokens[position].getType() != TokenType.More)
                return null;
            else
                sign = tokens[position].GetLexem();
            //getting right operand
            position++;
            var rightOperand = _analyzer.GetCurrentExpressions(tokens, ref position);
            //checking right bracket
            if (tokens[position].getType() == TokenType.RightBracket)
            {
                position++;
                return new ComparisonnExpressions(leftOperand, rightOperand, sign);
            }
            else
            {
                position = startPosition;
                throw new Exception("SyntaxErrorException in 'COMPARISON' declaration!");
            }
        }
    }

    //--------------------------------------------------------------------------
    //class to compare values
    //--------------------------------------------------------------------------
    class ComparisonnExpressions : IExpression
    {
        public IExpression LeftOperandd { get; private set; }
		public IExpression RightOperandd { get; private set; }
		public string Sign { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            var left = LeftOperandd.Execute(context);
            var right = RightOperandd.Execute(context);
            //checking if both operands are the same type
            if (left.GetType() != right.GetType())
                throw new Exception($"CompareException: Different variables types: '{left.GetType()}' and '{right.GetType()}'!");
            else
            {
                switch (Sign)
                {
                    case ">":
                        return (dynamic)left > (dynamic)right;
                    case "<":
                        return (dynamic)left < (dynamic)right;
                    case "<=":
                        return (dynamic)left <= (dynamic)right;
                    case ">=":
                        return (dynamic)left >= (dynamic)right;
                    case "!=":
                        return (dynamic)left != (dynamic)right;
                    case "==":
                        return (dynamic)left == (dynamic)right;
                    default:
                        return null;
                }
            }
        }
        //----------------------------------------------------------------------
        public ComparisonnExpressions(IExpression left, IExpression right, string sign)
        {
            LeftOperandd = left;
            RightOperandd = right;
            Sign = sign;
        }
    }
    //--------------------------------------------------------------------------
    
}
//------------------------------------------------------------------------------