﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class DoWhileExpressions : IExpression
    {
        public IExpression Condittion { get; private set; }
		public IExpression TrueExpressions { get; private set; }
		//----------------------------------------------------------------------
		public DoWhileExpressions(IExpression condition, IExpression trueExpression)
        {
            Condittion = condition;
            TrueExpressions = trueExpression;
        }
        //----------------------------------------------------------------------
        public object Execute(Contextt context)
        {
            //execute the context
            do
            {
                TrueExpressions.Execute(context);
            } while ((bool)Condittion.Execute(context));

            return null;
        }
    }
    //--------------------------------------------------------------------------
    class DoWhileExpressionsCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public DoWhileExpressionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking if the token is "while"
            if (tokens[position].getType() != TokenType.Do)
                return null;


            //adding all the tokens to the tree
            position++;
            var tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.While)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            var trueExpression = new SyntaxxAnalyzer().TreeBuilderr(tokensList);
            //checking if there is "end"
            if (tokens[position].getType() != TokenType.While)
                throw new Exception($"While statement syntax error: keyword 'while' is missing!");

            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"While statement syntax error: '(' is missing!");

            position++;
            var condition = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"While statement syntax error: ')' is missing!");

            position++;
            return new DoWhileExpressions(condition, trueExpression);

        }
    }
}
//------------------------------------------------------------------------------