﻿//------------------------------------------------------------------------------
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class TrueFalseExpressions : IExpression
    {
        public bool Vallue { get; private set; }
        //----------------------------------------------------------------------
        public object Execute(Contextt context)
        {
            return Vallue;
        }
        //----------------------------------------------------------------------
        public TrueFalseExpressions(bool value)
        {
            Vallue = value;
        }

		public override string ToString()
		{
			return Vallue.ToString();
		}
	}
    //--------------------------------------------------------------------------
    class TrueFalseExpressionsCreatorr :IExpressionCreator
    {
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.True && tokens[position].getType() != TokenType.False)
                return null;
            else
            {
                if (tokens[position].GetLexem() == "TRUE")
                {
                    position++;
                    return new TrueFalseExpressions(true);
                }
                else
                {
                    position++;
                    return new TrueFalseExpressions(false);
                }
            }
        }
    }
}
//------------------------------------------------------------------------------