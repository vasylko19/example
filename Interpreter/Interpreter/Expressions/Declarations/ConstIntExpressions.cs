﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    public class ConsttIntCreator : IExpressionCreator
    {
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.NumberConst)
                return null;
            else
                return new ConsttIntExpressions(Convert.ToInt32(tokens[position++].GetLexem()));
        }
    }


    //--------------------------------------------------------------------------
    //value of the int expression
    //--------------------------------------------------------------------------
    class ConsttIntExpressions : IExpression
    {
        public int Value { get; private set; }

		public ConsttIntExpressions(int value)
        {
            Value = value;
        }
        public object Execute(Contextt context)
        {
            return Value;
        }
    }
    //--------------------------------------------------------------------------
   
}
//------------------------------------------------------------------------------