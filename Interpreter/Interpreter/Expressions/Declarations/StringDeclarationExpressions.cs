﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{

    class StringDeclCreator : IExpressionCreator
    {
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.String)
                return null;
            else
            {
                var variable = tokens[++position];
                if (variable.getType() == TokenType.VariableName)
                {
                    position++;
                    return new StringDeclExpression(variable.GetLexem());
                }
                else throw new Exception("String Declaration Error!");
            }
        }
    }
    //--------------------------------------------------------------------------
    class StringDeclExpression : IExpression
    {
        public StringDeclExpression(string inputExpression)
        {
            InputData = inputExpression;
        }
        public string InputData { get; private set; }

        public object Execute(Contextt context)
        {
            context.Variables.Add(InputData, "");
            return null;
        }

       
	}
    //--------------------------------------------------------------------------
    
}
//------------------------------------------------------------------------------