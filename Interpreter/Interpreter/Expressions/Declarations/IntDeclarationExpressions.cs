﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //class used to declare integer
    //--------------------------------------------------------------------------
    class IntDeclExpression : IExpression
    {
        public IntDeclExpression(string inputExpression)
        {
            InputData = inputExpression;
        }
        public string InputData { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            context.Variables.Add(InputData, 0);
            return null;
        }
        //----------------------------------------------------------------------
      
    }
    //--------------------------------------------------------------------------
    class IntDeclCreator : IExpressionCreator
    {
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking token's type
            if (tokens[position].getType() != TokenType.Int)
                return null;
            else
            {
                var variable = tokens[++position];      //name of the variable
                if (variable.getType() == TokenType.VariableName)
                {
                    position++;
                    //creating int variable with the given name
                    return new IntDeclExpression(variable.GetLexem());
                }
                else throw new Exception("Int Declaration Error!");
            }
        }
    }
}
//------------------------------------------------------------------------------