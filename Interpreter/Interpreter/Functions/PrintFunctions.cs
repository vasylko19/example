﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{   
    class PrinttFunction : IExpression
    {
        public IExpression InputtData { get; private set; }
		//----------------------------------------------------------------------
		public PrinttFunction(IExpression input)
        {
            InputtData = input;
        }
        //----------------------------------------------------------------------
        public object Execute(Contextt context)
        {
            var arg = InputtData.Execute(context);
            SyntaxxAnalyzer.AnalyzerOutput += arg + "\n";

            return null;
        }
    }
    //--------------------------------------------------------------------------
    class PrintFunctionsCreatorr : IExpressionCreator
    {
        private readonly SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking token's type
            if (tokens[position].getType() != TokenType.Print)
                return null;
            //checking the syntax
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception("Print function syntax error: '(' is missing!");
            //expression to be printed
            position++;
            var data = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() == TokenType.RightParenthesis)
            {
                position++;
                return new PrinttFunction(data);
            }
            else throw new Exception("Print function syntax error: ')' is missing!");
        }
        //----------------------------------------------------------------------
        public PrintFunctionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        
    }
}
//------------------------------------------------------------------------------