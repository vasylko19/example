﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class IntToStringgCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public IntToStringgCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking token type
            if (tokens[position].getType() != TokenType.ToString)
                return null;
            //checking the syntax
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception("IntToString function syntax error: '(' is missing!");

            if (tokens[++position].getType() != TokenType.NumberConst)
                throw new Exception("IntToString function syntax error: int expected");
            var IntNum = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception("IntToString function syntax error: ')' is missing!");
            else
            {
                position++;
                return new IntToStringFunctions(IntNum);
            }
        }
    }

    //--------------------------------------------------------------------------
    //converts int to string
    //--------------------------------------------------------------------------
    class IntToStringFunctions :IExpression
    {
        public IExpression Vallue { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            return ((int)Vallue.Execute(context)).ToString();
        }
        //----------------------------------------------------------------------
        public IntToStringFunctions(IExpression value)
        {
            Vallue = value;
        }
    }
    //--------------------------------------------------------------------------
   
}
//------------------------------------------------------------------------------